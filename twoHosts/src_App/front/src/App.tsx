import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Paper, styled } from '@mui/material';
import axios from 'axios';


const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  textAlign: 'center',
  color: theme.palette.text.secondary,
  height: 60,
  lineHeight: '60px',
}));

interface Weather {
  date: any | null,
  temperatureC: string,
  temperatureF: string,
  summary: string,
}

function App() {


  const [error, setError] = useState<string | null>(null);
  const [text, setText] = useState<Array<Weather> | null>(null);

  const get = () => {
    axios<Array<Weather>>(`${process.env.REACT_APP_BACK_URL}/WeatherForecast`)
      .then(res =>{ 
        setError(null);
        setText(res.data);
      })
      .catch(error => setError(JSON.stringify(error)));
  };


  return (
    <div className="App">
      <Button onClick={get} variant='contained'>Клик GET</Button>
      {/* <Button onClick={put} variant='contained'>Клик POST</Button> */}
      {text && <Paper className="p-10" elevation={2}>
        <div>{"date: " + text[0].date}</div>
        <div>{"temperatureC: " + text[0].temperatureC}</div>
        <div>{"temperatureF: " + text[0].temperatureF}</div>
        <div>{"summary: " + text[0].summary}</div>
      </Paper>}
      {error && <Paper className="red p-10" elevation={2} >
        {error}
      </Paper >}
    </div>


  );
}

export default App;
